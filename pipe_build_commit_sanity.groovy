//Import Packages

import groovy.transform.Field

//@Library('novatel_pipelines@master')

String config_str = /
{
    "testConfigPath": "\\\\flsrv002\\oemtest1\\tools\\CDS\\tests\\TEAM_Integration\\Sandbox\\SaiHari\\SANITY\\TestConfig_Commit_Sanity.json",
    "emailRecipients": "saihari.tarimala@hexagon.com; sreekar.jakkula@hexagon.com; Jason.Wudkevich@hexagon.com",
}
/

pipe_Commit_Sanity(config_str)


@Field config
@Field testConfig

def pipe_Commit_Sanity(config_str) {
    currentBuild.result = 'SUCCESS'
    echo '${BUILD_NUMBER}'
    echo '${P4_CHANGELIST}'
    echo '${Branch}'
    echo '${Branch04}'
    currentBuild.displayName = "${BUILD_NUMBER}@${Branch04}@${P4_CHANGELIST}"
    
    config = readJSON text: config_str

    node('CDS_TestNode73') {
        try {
            stage('Load Config') {
           
                testConfig = readJSON file: config.testConfigPath
           
                def testCount = 0
           
                //Test config should contain required information to load receiers and run tests. Filter out invalid entries.
                testConfig.each { node, nodeConfig ->
                    nodeConfig.receivers.eachWithIndex { receiver, receiverConfig, index ->
                        boolean isValid = receiverConfig.oemcard &&
                                receiverConfig.enclosure && 
                                receiverConfig.model &&
                                receiverConfig.firmware &&
                                receiverConfig.test &&
                                receiverConfig.test.name &&
                                receiverConfig.test.target
           
                        if (!isValid) {
                            echo "WARNING: Unable to load test config for $node-R$receiver. Test will exit pipeline."
                            nodeConfig.receivers.remove(receiver)
                        }
                    }
                    // each receiver block in the test config is one test
                    testCount += nodeConfig.receivers.size()
                }
           }
       }
       catch (error){
           echo error.message
           //currentBuild.currentResult = 'FAILURE'
       }
       try { 
           stage('Copy FW') {
               if (currentBuild.currentResult == 'SUCCESS') {
                   if (config.skipTests && config.skipTests.toBoolean()) {
                       echo 'skipTests flag is true, skipping Sanity stage'
                       return
                   }
           
                   def tasks = [:]
                   // from the test config build Groovy map of tasks to execute tests
                   testConfig.each { node, nodeConfig ->
                       nodeConfig.receivers.eachWithIndex { receiver, receiverConfig, index ->
                           def firmware = receiverConfig.firmware
                           echo "Firmware is : $firmware"
                           def model = receiverConfig.model
                           def testName = receiverConfig.test['name']
           
                           tasks["$node-R$receiver-$testName"] = {
                               copyFW(node, receiver, firmware, model, testName)
                           }
                       }
                   }
                   parallel tasks
               }
           }
       }
       catch (error) {
           echo error.message
           //currentBuild.currentResult = 'FAILURE'		   
       }      
       try {
           stage('Sanity') {
               if (currentBuild.currentResult == 'SUCCESS') {
                   if (config.skipTests && config.skipTests.toBoolean()) {
                       echo 'skipTests flag is true, skipping Sanity stage'
                       return
                   }
           
                   def tasks = [:]
           
                   // from the test config build Groovy map of tasks to execute tests
                   testConfig.each { node, nodeConfig ->
                       nodeConfig.receivers.eachWithIndex { receiver, receiverConfig, index ->
                           def testName = receiverConfig.test['name']
                           def testTarget = receiverConfig.test['target']
                           def testOwner = receiverConfig.test['owner']
           
                           tasks["$node-R$receiver-$testName"] = {
                               runTest(node, receiver, testName, testTarget)
                           }
                       }
                   }
           
                   timeout(time: 20, unit: 'MINUTES') {
                       parallel tasks
                   def sanity_result = 'SUCCESS'
                   writeFile file: "sanity_result.txt", text: sanity_result					   
                   }
               }
           }
       }
       catch (error) {
           echo error.message
           //currentBuild.currentResult = 'FAILURE'		   
           def sanity_result = 'FAILURE'
           writeFile file: "sanity_result.txt", text: sanity_result 		   
       }
       try {
           stage('CLINFO') {
               if (currentBuild.currentResult == 'SUCCESS') {
                    withCredentials([usernamePassword(credentialsId: 'oembuild', passwordVariable: 'userpass', usernameVariable: 'username')]) {
                        bat label: 'Generate p4_changelists.txt',
                                script: """python "\\\\flsrv001\\OEMTest1\\tools\\CDS\\Scripts\\Jira\\jira_tools.py" clinfo ${username} ${userpass} ${P4_CHANGELIST}"""
                        print("Stashing p4changelists")
                        stash name: "p4changelists", includes: 'p4_changelists.txt'
                   }
               }
           }
       }
       catch (error) {
           echo error.message
           //currentBuild.currentResult = 'FAILURE'		   
       }
       try {
           stage('JIRA') {
               if (currentBuild.currentResult == 'SUCCESS') {
                    withCredentials([usernamePassword(credentialsId: 'svc-jira-automation', passwordVariable: 'userpass', usernameVariable: 'username')]) {
                            def result = readFile file: "sanity_result.txt" 
                            def pipeline_url = """http://bldsrv07:8080/blue/organizations/jenkins/${env.JOB_NAME}/detail/${env.JOB_NAME}/${currentBuild.number}/pipeline"""
                            def deploy_dir = """\\\\flsrv002\\CDS_M7Main\\$JOB_NAME\\${Branch04}\\$BUILD_NUMBER"""
                            print("Unstashing p4changelists")						
                            unstash "p4changelists"
                            
                            bat label: 'Add Jira Comments',
                                   script: """python "\\\\flsrv002\\oemtest1\\tools\\CDS\\tests\\TEAM_Integration\\Sandbox\\SaiHari\\SANITY\\call_jira.py" ${result} ${JOB_URL} ${BUILD_URL} ${pipeline_url} ${deploy_dir} ${username} ${userpass} """
                        
                   }
               }
           }
       }
       catch (error) {
           echo error.message
           //currentBuild.currentResult = 'FAILURE'		   
       }	   
       try {
           stage('Deploy Results') {
               
               def tasks = [:]
           
               testConfig.each { node, nodeConfig ->
                   nodeConfig.receivers.eachWithIndex { receiver, receiverConfig, index ->
                       def testName = receiverConfig.test['name']
                       def oemcard = receiverConfig.oemcard
                       def enclosure = receiverConfig.enclosure
                       
                       tasks["$node-R$receiver-$testName"] = {
                           deploy(node, testName, oemcard, enclosure)
                       }
                   }
               }
               parallel tasks
           }
       }
       catch (error) {
           echo error.message
          // currentBuild.currentResult = 'FAILURE'		   
       }
       //stage_email(config) {
       try {
           stage('Send Email') {
               def emailSubject = "[CDS] [JENKINS] (${currentBuild.currentResult}) ${env.JOB_NAME} Build #${currentBuild.number}"
               def emailBody = """${env.JOB_NAME} - Build #${currentBuild.number} - ${currentBuild.currentResult}:
    
    Jenkins Job: ${JOB_URL}
    Build URL: ${BUILD_URL}
    Pipeline: http://bldsrv07:8080/blue/organizations/jenkins/${env.JOB_NAME}/detail/${env.JOB_NAME}/${
                    currentBuild.number
               }/pipeline
    
    Test Config: ${config.testConfigPath}
    
    Deployment Directory: \\\\flsrv002\\CDS_M7Main\\$JOB_NAME\\${Branch04}\\$BUILD_NUMBER
    """ //+ destDir()
               // Email Recipients
               //config.emailRecipients.addAll(config.emailRecipients.split(';'))     		   
               emailext(
                   to: config.emailRecipients,//.join(';'),
                   body: emailBody,
                   recipientProviders: [],
                   subject: emailSubject
                   )
           }
       }
       catch (error) {
           echo error.message
          // currentBuild.currentResult = 'FAILURE'		   
       }
    }
    // }
}

def copyFW(nodeName, receiver, firmware, model, testName) {
    node(nodeName) {
        ws("${getRootWorkspace(nodeName)}\\$testName") {
            cleanWs()   
            
            def fwPath = null
            
            if (firmware.toLowerCase().contains('m7')) {
                def jenkinsJobUrl = "http://bldsrv07:8080/job"
                def jobName = "M7_BUILD_COMMIT_TRIGGER/"
                def buildNumber = "$jenkinsJobUrl/$jobName/lastSuccessfulBuild/buildNumber".toURL().text
                echo"***********************************************"
                def m7NonDebugArtifacts = "$firmware".split('\\\\')[-1]
                echo "The FW is: $m7NonDebugArtifacts"
                
                echo "Downloading firmware from Jenkins job: $jobName"
            
                def returnVal = bat returnStatus: true, script: "wget $jenkinsJobUrl/$jobName" +
                        "$buildNumber/artifact/product_m7/nondebug/$m7NonDebugArtifacts -P product_m7\\nondebug\\"
                echo "The FW: $returnVal"
                if (returnVal == 0) {
                    fwPath = pwd() + '\\product_m7\\nondebug\\'
                }
            } else {
                echo "Checking if given firmware path exists: $firmware"
                returnVal = 0 
            
                // def returnVal = bat returnStatus: true, script: """
// if exist \"$firmware\"(
    // exit 0
// ) else (
    // exit -1
// )    
// """
                if (returnVal == 0) {
                    echo "Firmware path exists: $firmware"
                
                    dir("product_m7\\nondebug") {
                        returnVal = bat returnStatus: true, script: "copy /y \"$firmware\" "
                    }
                
                    if (returnVal == 0) {
                        fwPath = pwd() + '\\product_m7\\nondebug\\'
                    }
                } else {
                    echo "Firmware path doesn't exist: $firmware"
                }
            }           
        }
    }
}

def runTest(nodeName, executorNumber, testName, testTarget) {
    node(nodeName) {
        ws("${getRootWorkspace(nodeName)}\\$testName") {    
            testTarget = testTarget.replaceAll('NODE_NAME', nodeName)
            testTarget = testTarget.replaceAll('EXECUTOR_NUMBER', executorNumber)
            testTarget = testTarget.replaceAll('BUILD_NUMBER', env.BUILD_NUMBER)
            testTarget = testTarget.replaceAll('JOB_NAME', env.JOB_NAME)

            bat testTarget
        }
    }
}

def deploy(nodeName, testName, oemcard, enclosure) {
    node(nodeName) {
        ws("${getRootWorkspace(nodeName)}\\$testName") {
            //def branch = "${Branch04}"
            def destDir = "\\\\flsrv002\\CDS_M7Main\\$JOB_NAME\\$Branch04\\$BUILD_NUMBER\\$oemcard\\$enclosure"

            // use robocopy to deploy (mirror) test results
            robocopy(". $destDir /MIR /FFT /Z /XA:H /W:5")
        }
    }
}

// def get_deploy_dir_string() {
    // //Artifacts path
    // return "\\\\flsrv002\\CDS_M7Main\\$JOB_NAME\\$BUILD_NUMBER"
// }

def getRootWorkspace(nodeName) {
    for (node in Jenkins.instance.nodes) {
        if (!nodeName.equals('master') &&
                node.name.compareTo(nodeName) == 0) {
            return node.rootPath.toString() + '\\workspace'
        }
    }
}

def robocopy(cmd) {
    print("Beginning Robocopy")
    // robocopy uses non-zero exit code even on success, status below 3 is fine
    def status = bat returnStatus: true, script: "ROBOCOPY ${cmd}"
    println "ROBOCOPY returned ${status}"
    if (status < 0 || status > 3) {
        error("ROBOCOPY failed")
    }
    print("Done Robocopy")

}